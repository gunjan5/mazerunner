import java.util.TreeSet;

public class Cell {
	
	 public boolean up;     // is there a wall to up of cell i, j
	 public boolean right;
	 public boolean down;
	 public boolean left;
     public boolean visited;
     TreeSet<Cell> adj_list = new TreeSet<Cell>();
     
     public int cellID_x, cellID_y;
	
 	public boolean cellVisted; 
 	public String cellSolvedPath; //this will leave a "#"
 	public int cellBFSorDFSValue; //
 	public String cellCurrentValue;
	
     public String topLeftCorner;
     public String topRightCorner;
     public String botLeftCorner;
     public String botRightCorner;
     public String leftWall;
     public String rightWall;
     public String topWall;
     public String botWall;
	

 	public String topRow = "";
 	public String midRow = "";
 	public String botRow = "";
 	
	public Cell(int x, int y){
		this.cellID_x = x;
		this.cellID_y = y;
		up = true;     // is there a wall to up of cell i, j
		right = true;
		down = true;
		left = true;
	    visited = false;
	    topLeftCorner = "+";
		topRightCorner = "+";
		botLeftCorner = "+";
		botRightCorner = "+";
		
		topWall = "-";
		botWall = "-";
		
		leftWall = "|";
		rightWall = "|";
		
		cellCurrentValue = " ";
	}
	
	public void setXY(int x, int y) {
		this.cellID_x = x;
		this.cellID_y = y;
	}
	public void removeTopWall(){
		topWall = " ";
		up = false;
	}

	public void removeBotWall(){
		botWall = " ";
		down = false;
	}
	
	public void removeLeftWall(){
		leftWall = " ";	
		left = false;
	}
	
	public void removeRightWall(){
		rightWall = " ";
		right = false;
	}
	
	
public void cellStringUpdate(){
		
		//idea is to draw only left and top 
		

		if(cellID_x == Maze.N-1 && cellID_y == Maze.N -1){  //all cells in last row and column
			topLeftCorner = "";
			leftWall = "";
			botLeftCorner = "";
			botWall = "";
			botRightCorner = "";
		}		
		else{
			//(cellID_x != mazeSize-1 && cellID_y != mazeSize -1){    //all cells except last row and last column
			topRightCorner = "";
			rightWall = "";
			botRightCorner = "";
			botWall = "";
			botLeftCorner = "";
		}

		//cellCurrentValue += cellID;
		
		topRow += topLeftCorner + topWall + topRightCorner;
		midRow += leftWall + cellCurrentValue + rightWall;
		botRow += botLeftCorner + botWall + botRightCorner;
	}
	
	
	// the drawCell works on positioning the "cell" relative to its ID number
	public void drawCell(){
		System.out.println(topLeftCorner + topWall + topRightCorner);
		System.out.println(leftWall + " " + rightWall);
		System.out.println(botLeftCorner + botWall + botRightCorner);
	}
}
