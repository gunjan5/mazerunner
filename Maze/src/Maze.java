
public class Maze {
    public static int N;                 // dimension of maze
    Cell cell[][];     // is there a wall to up of cell i, j
    public String mazeRowOneString = "";
	public String mazeRowTwoString = "";
	public String mazeRowThreeString = "";
    private int visitedCounter= 0;
    private boolean done = false;

    public Maze(int N) {
        this.N = N;
        init();
        generate();
    }

    private void init() {
    	int totalCells = N*N;
    	cell = new Cell[N+2][N+2];
    	for ( int i=0;i<N+2;i++){
    		for(int j=0;j<N+2;j++){
    			cell[i][j] = new Cell(i,j);
    		}
    	}
        // initialize border cells as already visited
        //visited = new boolean[N+2][N+2];
        for (int x = 0; x < N+2; x++) {
        	System.out.println("in loop x= " +x);
        	cell[x][0].visited = true;
        	cell[x][N+1].visited = true;
        }
        for (int y = 0; y < N+2; y++) {
        	cell[0][y].visited = true;
        	cell[N+1][y].visited = true;
        }


//        // initialze all walls as present
//        up = new boolean[N+2][N+2];
//        right  = new boolean[N+2][N+2];
//        down = new boolean[N+2][N+2];
//        left  = new boolean[N+2][N+2];
//        for (int x = 0; x < N+2; x++)
//            for (int y = 0; y < N+2; y++){
//                up[x][y] = right[x][y] = down[x][y] = left[x][y] = true;
//            }
    }


    // generate the maze
    private void generate(int x, int y) {
        cell[x][y].visited = true;

        // while there is an unvisited neighbor
        while (!(cell[x][y+1].visited) || !(cell[x+1][y].visited) || !(cell[x][y-1].visited) || !(cell[x-1][y].visited)) {

            // pick random neighbor 
            while (true) {
                double r = Math.random();
                if (r < 0.25 && !(cell[x][y+1].visited)) {
                    cell[x][y].removeTopWall();
                    cell[x][y+1].removeBotWall();
                    cell[x][y].adj_list.add(cell[x][y+1]);
                    cell[x][y+1].adj_list.add(cell[x][y]);
                    generate(x, y + 1);
                    break;
                }
                else if (r >= 0.25 && r < 0.50 && !(cell[x+1][y].visited)) {
                    cell[x][y].removeRightWall();
                    cell[x+1][y].removeLeftWall();
                    cell[x][y].adj_list.add(cell[x+1][y]);
                    cell[x+1][y].adj_list.add(cell[x][y]);
                    generate(x+1, y);
                    break;
                }
                else if (r >= 0.5 && r < 0.75 && !(cell[x][y-1].visited)) {
                    cell[x][y].removeBotWall();
                    cell[x][y-1].removeTopWall();
                    cell[x][y].adj_list.add(cell[x][y-1]);
                    cell[x][y-1].adj_list.add(cell[x][y]);
                    generate(x, y-1);
                    break;
                }
                else if (r >= 0.75 && r < 1.00 && !(cell[x-1][y].visited)) {
                    cell[x][y].removeLeftWall();
                    cell[x-1][y].removeRightWall();
                    cell[x][y].adj_list.add(cell[x-1][y]);
                    cell[x-1][y].adj_list.add(cell[x][y]);
                    generate(x-1, y);
                    break;
                }
            }
        }
    }

    // generate the maze starting from lower right
    private void generate() {
        generate(1, 1);
//
//
//        // delete some random walls
//        for (int i = 0; i < N; i++) {
//            int x = (int) (1 + Math.random() * (N-1));
//            int y = (int) (1 + Math.random() * (N-1));
//            up[x][y] = down[x][y+1] = false;
//        }
//
//        // add some random walls
//        for (int i = 0; i < 10; i++) {
//            int x = (int) (N / 2 + Math.random() * (N / 2));
//            int y = (int) (N / 2 + Math.random() * (N / 2));
//            right[x][y] = left[x+1][y] = true;
//        }

     
    }
    
	//---------------- this section is for drawing thee maze
	
	public void drawMaze(){
		
		for(int i=0; i<N; i++){
			for(int j=0; j<N; j++){

				cell[i][j].cellStringUpdate();
				mazeRowOneString += cell[i][j].topRow;
				mazeRowTwoString += cell[i][j].midRow;
				mazeRowThreeString += cell[i][j].botRow;	
			}
			
				System.out.println(mazeRowOneString);
				System.out.print(mazeRowTwoString);
				System.out.println(mazeRowThreeString);
				
			mazeRowOneString   = "";
			mazeRowTwoString   = "";
			mazeRowThreeString = "";
		}
		
	}
    
    
}

